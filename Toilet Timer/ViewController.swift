//
//  ViewController.swift
//  Toilet Timer
//
//  Created by Nathan Schultz on 10/22/16.
//  Copyright © 2016 Nathan Schultz. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {

    weak var optionalInterval : Timer?
    var history = [(Date, Date)]()
    var currentTimer : (startTime: Date?, endTime: Date?) = (nil, nil)
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var timeDisplay: UILabel!
    
    @IBAction func startButtonPressed(_ sender: AnyObject) {
        print("started")
        if optionalInterval == nil {
            currentTimer = (Date(), nil)
            optionalInterval = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ViewController.updateTimerDisplay), userInfo: nil, repeats: true)
            UserDefaults.standard.set(currentTimer.startTime, forKey: "start")
        }
    }
    
    @IBAction func stopTimer(_ sender: AnyObject) {
        if let interval = optionalInterval {
            interval.invalidate()
            var (startTime, endTime) = currentTimer
            if let start = startTime {
                endTime = Date()
                currentTimer = (start, endTime)
                saveTime()
            }
        }
    }

    @IBAction func resetHistory(_ sender: AnyObject) {
        history = [(Date, Date)]()
        UserDefaults.standard.set([[Date]](), forKey: "history")
        table.reloadData()
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return history.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "Cell")
        cell.textLabel?.text = displayHistory(pairOfDates: history[indexPath.row])
        cell.textLabel?.numberOfLines=0 // line wrap
        cell.textLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
        return cell
    }
    

    func displayHistory ( pairOfDates: (Date, Date)) -> String {
        let opStart : Date? = pairOfDates.0
        let opEnd :Date? = pairOfDates.1
        let startTime = pairOfDates.0
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        formatter.dateStyle = .short
        return "You were in the restroom for \(getElapsedTime(durationDates: (opStart, opEnd))) seconds on \(formatter.string(from: startTime))"
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //load exisiting dates from userDefaults and start the timer if there is a start
        
        let startObject = UserDefaults.standard.object(forKey: "start")
        
        if optionalInterval == nil && startObject as? Date != nil {
            currentTimer = (startObject as! Date?, nil)
            optionalInterval = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ViewController.updateTimerDisplay), userInfo: nil, repeats: true)
        }
        
        let historyObject = UserDefaults.standard.object(forKey: "history")

        if let userDefaultsHistory = historyObject {
            let userDefaultHistorylength = (userDefaultsHistory as AnyObject).count
            if userDefaultHistorylength! > history.count {
                let compatableHistory = userDefaultsHistory as! [[Date]]
                history = compatableHistory.map{
                    (dateArray) -> (Date, Date) in
                    let dateTuple : (Date, Date) = (startTime: dateArray[0], endTime: dateArray[1])
                    return dateTuple
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func saveTime (){
        if currentTimer.startTime != nil && currentTimer.endTime != nil {
            let (start, end) = currentTimer
            history.append(start!, end!)
            let compatibleHistory : [[Date]] = history.map{
                (dateTuple) -> [Date] in
                let dateArray : [Date] = [dateTuple.0, dateTuple.1]
                return dateArray
            }
            UserDefaults.standard.set(compatibleHistory, forKey: "history")
            UserDefaults.standard.set(nil, forKey: "start")
        }
        currentTimer = (nil, nil)
        table.reloadData()
        updateTimerDisplay()
    }
    
    func getElapsedTime (durationDates: (Date?, Date?)) -> Int {
        let (opStart, opEnd) = durationDates
        if opStart == nil {
            return 0
        } else {
            let start : Date = opStart!
            var end : Date = Date()
            if opEnd != nil {
                end = opEnd!
            }
            return Int(end.timeIntervalSinceReferenceDate - start.timeIntervalSinceReferenceDate)
        }
    }

    func updateTimerDisplay () {
        print("updating...")
        timeDisplay.text = prettyPrintTime(numSeconds: getElapsedTime(durationDates: currentTimer))
    }
    
    func prettyPrintTime (numSeconds: Int) -> String {
        let seconds = numSeconds % 60
        let minutes = (numSeconds / 60) % 60
        let hours = (numSeconds / 3600)
        return String(format: "%0.2d:%0.2d:%0.2d", hours, minutes, seconds)
    }
    
}

