# README #

### This app is designed to help you spend less time in the bathroom looking at your phone ###

This is an app I started over the weekend. So far it has an app icon and a timer. When you press play it starts the timer, which keeps going even if you leave the app or force quit the app. When you open it again the timer will still be going because it's start time is saved in userDefaults. When you press stop it will save the timer details to the history table. You can clear the history with the trash icon.

## Yet to be implemented: 
- [ ] Notifications that repeatedly remind you that you should get out of the bathroom.
- [ ] The slider will allow you to adjust the frequency of notifications
- [ ] A splash screen giving instructions 
- [ ] Move history to a separate view
- [ ] Generally add design/color/animation to make it look fancier